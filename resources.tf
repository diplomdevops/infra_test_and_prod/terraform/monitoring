# Запускаем инстанс для grafana
resource "aws_instance" "grafana_loki" {
  # с выбранным образом
  ami = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.monitoring.id]
  user_data              = file("user_data.sh")
  key_name               = var.ssh_key_name
  tags = {
    Name = var.name_grafana
  }
  lifecycle {
    create_before_destroy = true
  }

}
# Запускаем инстанс для prometheus
resource "aws_instance" "prometheus" {
  # с выбранным образом
  ami = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы)
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.monitoring.id]
  user_data              = file("user_data.sh")
  key_name               = var.ssh_key_name
  tags = {
    Name = var.name_prometheus

  }
  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_security_group" "monitoring" {
  name = var.name_monitoring

  dynamic "ingress" {
    for_each = var.allow_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.cidr_blocks_ingress
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.cidr_blocks_egress
  }

  tags = {
    Name = var.name_monitoring
  }
}
