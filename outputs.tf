# Выведем IP адрес сервер prometheus
output "prometheus" {
  description = "prometheus"
  value       = aws_instance.prometheus.public_ip
}

# Выведем IP адрес сервер grafana
output "grafana_loki" {
  description = "grafana_loki"
  value       = aws_instance.grafana_loki.public_ip
}
