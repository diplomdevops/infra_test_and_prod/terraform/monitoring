variable "region" {
  description = "Please enter AWS region to deploy server"
  default     = "us-east-1"
}

variable "name_profile" {
  description = "Please enter name of your profile"
  default     = "iceforest"
}

variable "name_monitoring" {
  description = "Please enter name of your monitoring"
  default     = "monitoring"
}

variable "name_grafana" {
  description = "Please enter name of your monitoring"
  default     = "grafana_loki"
}

variable "name_prometheus" {
  description = "Please enter name of your monitoring"
  default     = "prometheus"
}

variable "instance_type" {
  description = "Please enter instance type"
  default     = "t2.micro"
}

variable "allow_ports" {
  description = "list of ports to open server"
  type        = list(any)
  default     = ["80", "22", "81", "3000", "9090", "9093", "9094", "9100", "24224", "3100", "9080"]
}

variable "cidr_blocks_ingress" {
  type    = list(any)
  default = ["0.0.0.0/0"]
}

variable "cidr_blocks_egress" {
  type    = list(any)
  default = ["0.0.0.0/0"]
}


variable "ssh_key_name" {
  description = "Please enter your ssh key name"
  default     = "iceforest"
}
